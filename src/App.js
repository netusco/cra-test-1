import React, { Component } from 'react';
import { ExternalComponent } from 'webpack-external-import';
import logo from './logo.svg';
import './App.css';

const App = () => (
    <div className="App">
    <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {
            <ExternalComponent
                interleave={__webpack_require__// eslint-disable-line no-undef
                .interleaved("exporter-site/SharedTitleComponent")
                .then(() => __webpack_require__("SharedTitleComponent")) } // eslint-disable-line no-undef
                export="SharedTitle"
                title="Some Heading"
            />
        }
        <p>
        Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
        >
        Learn React
        </a>
    </header>
    </div>
);

export default App;
