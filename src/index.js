import { corsImport } from 'webpack-external-import';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
  // eslint-disable-line no-undef

corsImport(`http://localhost:3004/importManifest.js?${Date.now()}`).then(() => {
  console.log('Manifest loaded from localhost:3004');
  ReactDOM.render(<App />, document.getElementById("root"));
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
